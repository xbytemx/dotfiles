alias two="xrandr --setprovideroutputsource 3 0 && xrandr --setprovideroutputsource 4 0 && xrandr --output DVI-I-2-1 --mode 1920x1080 --pos 0x0 --rotate normal --output eDP-1 --primary --mode 1920x1080 --pos 3840x0 --output DVI-I-3-2 --mode 1920x1080 --pos 1920x0 --rotate normal"
alias playalert='pacmd play-file /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga $(pacmd list-sinks | grep -E "index" | grep -oE ".$" | tail -n1)'
alias wanip="curl https://diagnostic.opendns.com/myip"
alias bloodhound2='docker run -it -p 7474:7474 -e DISPLAY=unix:0 -v /tmp/.X11-unix:/tmp/.X11-unix --device=/dev/dri:/dev/dri -v $HOME/tools/bloodhound-data:/data --name bloodhound belane/bloodhound'
alias teams="systemd-run --scope -p MemoryLimit=2000M teams"
alias httpx="http --verify=no --proxy=https:http://127.0.0.1:8080 --proxy=http:http://127.0.0.1:8080 "

GITHOME="$HOME/git/"

function dirsearch() {
    python3 $GITHOME/dirsearch/dirsearch.py -u $1 --random-agent -e php,txt,html -t 50 -f -x 400,403
}

function dirsearchw() {
    python3 $GITHOME/dirsearch/dirsearch.py -u $1 --random-agent -e php,txt,html -t 50 -w $2 -f --plain-text-report="$1.dirsearch_wordlist.txt" -x 400,403
}

function crtsh() {
    curl -s https://crt.sh/?q\=%.$1\&output\=json | jq -r '.[].name_value' | sed 's/\*\.//g' | sort -u
}

function ipinfo() {
    curl http://ipinfo.io/$1
}

function ncx() {
    rlwrap ncat -l -n -vv -p $1 -k
}

function slowh() {
    slowhttptest -c 3000 -H -g -i 10 -r 200 -t GET -x 24 -p 2 -u $1
}
function slowb() {
    slowhttptest -c 3000 -B -g -i 110 -r 200 -s 8192 -t FAKEVERB -x 10 -p 3 -u $1
}

function ddos1() {
    /usr/sbin/hping3 -c 20000 -d 120 -S -w 64 -p $2 --flood --rand-source $1
}

function dosdns() {
    /usr/sbin/mz -A rand -B $1 -t dns "q=miau.cat" -c 10000000
}

function genpass() {
    tr -dc 'a-zA-Z0-9_#@.-' < /dev/urandom | head -c ${1:-14};
    echo;
}

function genrandommac() {
    openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'
}

function man() {
    env \
        LESS_TERMCAP_mb=$'\e[01;31m' \
        LESS_TERMCAP_md=$'\e[01;31m' \
        LESS_TERMCAP_me=$'\e[0m' \
        LESS_TERMCAP_se=$'\e[0m' \
        LESS_TERMCAP_so=$'\e[01;44;33m' \
        LESS_TERMCAP_ue=$'\e[0m' \
        LESS_TERMCAP_us=$'\e[01;32m' \
        man "$@"
}

function bloodhound() {
    docker run --rm -it -p 7474:7474 -p 7687:7687 --name neo4jbloodhound -d neo4j:3.5.14
    ~/tools/BloodHound-linux-x64/BloodHound --no-sandbox && docker kill neo4jbloodhound
}

function fzf-lovely() {
    if [ "$1" = "h" ]; then
        fzf -m --reverse --preview-window down:20 --preview '[[ $(file --mime {}) =~ binary ]] &&
        echo {} is a binary file ||
        (batcat --style=numbers --color=always {} ||
        highlight -O ansi -l {} ||
        coderay {} ||
        rougify {} ||
        cat {}) 2> /dev/null | head -500'
    else
        fzf -m --preview '[[ $(file --mime {}) =~ binary ]] &&
        echo {} is a binary file ||
        (batcat --style=numbers --color=always {} ||
        highlight -O ansi -l {} ||
        coderay {} ||
        rougify {} ||
        cat {}) 2> /dev/null | head -500'
    fi
}
