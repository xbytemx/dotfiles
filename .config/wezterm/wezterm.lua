local wezterm = require("wezterm")
local config = {}

if wezterm.config_builder then
	config = wezterm.config_builder()
end
config.show_update_window = false
config.check_for_updates = false
config.front_end = "WebGpu"
-- config.window_background_opacity = 0.9
config.default_cursor_style = "BlinkingBar"
config.color_scheme = "Dracula"
config.font_size = 17.0
config.audible_bell = "Disabled"
config.term = "xterm-256color"
config.hide_tab_bar_if_only_one_tab = true
config.font = wezterm.font_with_fallback({
	"Fira Code",
	"FontAwesome",
	"WenQuanYi Micro Hei",
})

return config
