#!/bin/bash

if [[ $(bspc query -M --names | wc -l) == 1 ]]; then
  bspc monitor $(bspc query -M --names) -d 1 2 3 4 5 6
else
  shift=0
  for monitor in $(bspc query -M --names)
  do 
    bspc monitor $monitor -d $(($shift*3+1)) $(($shift*3+2)) $(($shift*3+3))
    let shift++
  done
fi
