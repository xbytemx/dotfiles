# Requirements

This dot files requires the following binaries for the different tasks:

```bash
apt install feh bspwm sxhkd compton picom suckless-tools brightnessctl alsa-utils flameshot kitty fonts-firacode fonts-wqy-microhei fonts-font-awesome bat blueman shorewall dnscrypt-proxy i3lock-fancy rofi tmux polybar fonts-noto-color-emoji psmisc
```

bspwm and sxhkd are part of the vm combo. Shorewall and dnscrypt-proxy are just and extra tools I dont want to forget to install. There is a conflict with picom on debian testing, maybe in the future is gonna be removed.

- feh is used to handle and set the default background, which always point to ~/img/background.jpg
- suckless-tools includes the wmname, which is used to set the name of the WM for the java applications
- brightnessctl is used with the multimedia buttons aka xfree86 actions, to control the brightness
- alsa-utis contains amixer, which is also used with the multimedia actions such as mute and increse/decrease the volume
- flameshot is used as a screen capture tool
- i3lock-fancy is a tool used to lock the screen in a nice looking way
- rofi is used as a dmenu bar to execute desktop-defined applications
- the fonts installed are used by polybar and kitty. Polybar uses the icons and kitty the ligatures
